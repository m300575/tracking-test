# ICON issue tracking test

Playground to test different options for the [ICON development workflow](https://pad.gwdg.de/XYqj_lnRQ56bdrW-Mui6dg).

## Different overview stuctures

* The [Sapphire issue borad](https://gitlab.dkrz.de/m300575/tracking-test/-/boards)
* Milestones for larger experiments (e.g. [NextGEMS production run](https://gitlab.dkrz.de/m300575/tracking-test/-/milestones/1#tab-issues))
* Labels to sort issues (e.g. [`DestinE`](https://gitlab.dkrz.de/m300575/tracking-test/-/issues/?sort=created_date&state=opened&label_name%5B%5D=DestinE&first_page_size=20))


New feature.
